FROM gitpod/workspace-c:latest
USER gitpod

SHELL [ "bash" , "-lic" ]

# this was because they were using alpine
COPY scripts/gitpod-guix.sh /gitpod-guix.sh 
RUN sudo chmod +x /gitpod-guix.sh


# Install Guix
RUN sudo mkdir -p /gnu/store \
	&& sudo addgroup guixbuild \
	&& sudo addgroup guix-builder \
	&& sudo chgrp guix-builder -R /gnu/store \
	&& sudo chmod 1777 /gnu/store \
	&& for i in `seq -w 1 10`; do \
			#sudo adduser -G guixbuild -h /var/empty -s `which nologin` -S guixbuilder$i; \
			sudo useradd -g guixbuild --groups guixbuild -d /var/empty -s `which nologin` --system guixbuilder$i; \
		done \
	&& wget -O - https://ftp.gnu.org/gnu/guix/guix-binary-1.4.0.x86_64-linux.tar.xz | sudo tar -xJv -C / \
	&& sudo mkdir -p /root/.config/guix \
	&& sudo ln -sf /var/guix/profiles/per-user/root/current-guix /root/.config/guix/current \
	&& sudo mkdir -p /usr/local/bin \
	&& sudo ln -s /var/guix/profiles/per-user/root/current-guix/bin/guix /usr/local/bin/ \
	&& sudo mkdir -p /usr/local/share/info \
	&& for i in /var/guix/profiles/per-user/root/current-guix/share/info/*; do \
			sudo ln -s $i /usr/local/share/info/; \
		done \
	&& sudo cp /root/.config/guix/current/etc/init.d/guix-daemon /etc/init.d/guix-daemon \
	&& sudo chmod 775 /etc/init.d/guix-daemon \
	&& source $GUIX_PROFILE/etc/profile \
        && sudo cat /root/.config/guix/current/share/guix/ci.guix.gnu.org.pub | sudo guix archive --authorize
	#&& sudo guix archive --authorize < /root/.config/guix/current/share/guix/ci.guix.gnu.org.pub

# can set it up, but can't seem to run it
RUN sudo apt install daemonize && \
	sudo update-rc.d guix-daemon defaults
	# sudo update-rc.d guix-daemon enable - seems to fail
	#sudo service guix-daemon start && \
	#guix package --install glibc-locales 

# update root - it will take too long
#RUN sudo guix pull

# only works for the root user
#RUN sudo /root/.config/guix/current/bin/guix-daemon --build-users-group=guixbuild && sudo guix pull
#RUN sudo /gitpod-guix.sh guix pull
#RUN sudo /gitpod-guix.sh guix package --install glibc-locales
#RUN sh -c '/gitpod-guix.sh guix package --install glibc-locales'

# install basic tools
#RUN guix package --install glibc-locales && \
#    guix package --install git 


# Install custom tools, runtime, etc. using apt-get
# For example, the command below would install "bastet" - a command line tetris clone:
#
# RUN sudo apt-get -q update && \
#     sudo apt-get install -yq bastet && \
#     sudo rm -rf /var/lib/apt/lists/*
#
# More information: https://www.gitpod.io/docs/config-docker/
